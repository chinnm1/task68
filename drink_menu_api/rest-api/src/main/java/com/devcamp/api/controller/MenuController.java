package com.devcamp.api.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.model.CMenu;

import com.devcamp.api.repository.IMenuRespository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class MenuController {
    @Autowired
    IMenuRespository pMenuRepository;

    @PostMapping("/menu") // Dùng phương thức POST
    public ResponseEntity<Object> createCMenu(@RequestBody CMenu pMenu) {
        try {

            Optional<CMenu> menuData = pMenuRepository.findById(pMenu.getId());
            if (menuData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Menu already exsit  ");
            }
            pMenu.setNgayTao(new Date());
            pMenu.setNgayCapNhat(null);
            CMenu _menu = pMenuRepository.save(pMenu);
            return new ResponseEntity<>(_menu, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified menu: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/menu/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updatemenuById(@PathVariable("id") long id,
            @RequestBody CMenu pMenu) {
        Optional<CMenu> menuData = pMenuRepository.findById(id);
        if (menuData.isPresent()) {
            CMenu menu = menuData.get();
            menu.setDonGia(pMenu.getDonGia());
            menu.setDuongKinh(pMenu.getDuongKinh());
            menu.setSalad(pMenu.getSalad());
            menu.setSize(pMenu.getSize());
            menu.setSoLuongNuocNgot(pMenu.getSoLuongNuocNgot());
            menu.setSuon(pMenu.getSuon());
            menu.setNgayCapNhat(new Date());

            try {
                return new ResponseEntity<>(pMenuRepository.save(menu), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified menu:" +
                                e.getMessage());
            }
        } else {
            System.out.println("------------NOT FOUND-----------------");
            return ResponseEntity.badRequest().body("Failed to get specified menu: " +
                    id + " for update.");
        }
    }

    @GetMapping("/all-menu") // Dùng phương thức GET
    public ResponseEntity<List<CMenu>> getAllmenu() {
        try {
            List<CMenu> menu = new ArrayList<CMenu>();
            pMenuRepository.findAll().forEach(menu::add);
            return new ResponseEntity<>(menu, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // Lấy voucher theo {id} KHÔNG dùng service
    @GetMapping("/menu/{id}") // Dùng phương thức GET
    public ResponseEntity<CMenu> getCmenuById(@PathVariable("id") long id) {
        Optional<CMenu> menuData = pMenuRepository.findById(id);
        if (menuData.isPresent()) {
            return new ResponseEntity<>(menuData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/menu/{id}") // Dùng phương thức DELETE
    public ResponseEntity<CMenu> deleteCmenuById(@PathVariable("id") long id) {
        try {
            pMenuRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Xoá/delete tất cả voucher KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/menus") // Dùng phương thức DELETE
    public ResponseEntity<CMenu> deleteAllCmenu() {
        try {
            pMenuRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
