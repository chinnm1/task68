package com.devcamp.api.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.model.CDrink;
import com.devcamp.api.repository.IDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DrinkController {
    @Autowired
    IDrinkRepository drinkRepository;

    // Tạo MỚI drink
    @PostMapping("/drinks") // Dùng phương thức POST
    public ResponseEntity<Object> createCDrink(@RequestBody CDrink pDrink) {
        try {

            Optional<CDrink> drinkData = drinkRepository.findById(pDrink.getId());
            if (drinkData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Drink already exsit  ");
            }
            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);
            CDrink _drinks = drinkRepository.save(pDrink);
            return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified drink: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/drink/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateDrinkById(@PathVariable("id") long id,
            @RequestBody CDrink pDrink) {
        Optional<CDrink> drinkData = drinkRepository.findById(id);
        if (drinkData.isPresent()) {
            CDrink drink = drinkData.get();
            drink.setMaNuocUong(pDrink.getMaNuocUong());
            drink.setTenNuocUong(pDrink.getTenNuocUong());
            drink.setPrice(pDrink.getPrice());
            drink.setNgayCapNhat(new Date());

            try {
                return new ResponseEntity<>(drinkRepository.save(drink), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified drink:" +
                                e.getMessage());
            }
        } else {
            System.out.println("------------NOT FOUND-----------------");
            return ResponseEntity.badRequest().body("Failed to get specified drink: " +
                    id + " for update.");
        }
    }

    @GetMapping("/all-drink") // Dùng phương thức GET
    public ResponseEntity<List<CDrink>> getAllDrink() {
        try {
            List<CDrink> drink = new ArrayList<CDrink>();
            drinkRepository.findAll().forEach(drink::add);
            return new ResponseEntity<>(drink, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // Lấy voucher theo {id} KHÔNG dùng service
    @GetMapping("/drink/{id}") // Dùng phương thức GET
    public ResponseEntity<CDrink> getCDrinkById(@PathVariable("id") long id) {
        Optional<CDrink> drinkData = drinkRepository.findById(id);
        if (drinkData.isPresent()) {
            return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/drink/{id}") // Dùng phương thức DELETE
    public ResponseEntity<CDrink> deleteCDrinkById(@PathVariable("id") long id) {
        try {
            drinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Xoá/delete tất cả voucher KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/drinks") // Dùng phương thức DELETE
    public ResponseEntity<CDrink> deleteAllCDrink() {
        try {
            drinkRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
