package com.devcamp.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "menu")
public class CMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "size")
    private char size;
    @Column(name = "duong_kinh")
    private int duongKinh;
    @Column(name = "suon")
    private int suon;
    @Column(name = "salad")
    private int salad;
    @Column(name = "so_luong_nuoc_ngot")
    private int soLuongNuocNgot;
    @Column(name = "don_gia")
    private int donGia;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_tao", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_cap_nhat", nullable = true)
    @LastModifiedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date ngayCapNhat;

    public CMenu() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public char getSize() {
        return size;
    }

    public void setSize(char size) {
        this.size = size;
    }

    public int getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(int duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public void setSuon(int suon) {
        this.suon = suon;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getSoLuongNuocNgot() {
        return soLuongNuocNgot;
    }

    public void setSoLuongNuocNgot(int soLuongNuocNgot) {
        this.soLuongNuocNgot = soLuongNuocNgot;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

}
