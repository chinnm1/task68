package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CCustomer;
import com.devcamp.pizza365.repository.CustomerRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("v1/user")
public class CustomerController {
    @Autowired
    CustomerRepository uCuserRespository;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllUsers() {
        List<CCustomer> userList = new ArrayList<CCustomer>();
        uCuserRespository.findAll().forEach(userElement -> {
            userList.add(userElement);
        });
        if (!userList.isEmpty()) {
            return new ResponseEntity<Object>(userList, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable(name = "id", required = true) Long id) {
        Optional<CCustomer> userFounded = uCuserRespository.findById(id);

        if (userFounded.isPresent()) {
            return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createUser(@RequestBody CCustomer userFromClient) {
        // TODO: process POST request

        try {
            CCustomer _user = new CCustomer(userFromClient.getFullname(), userFromClient.getEmail(),
                    userFromClient.getPhone(),
                    userFromClient.getAddress());

            Date _now = new Date();
            _user.setCreated(_now);
            _user.setUpdated(null);
            uCuserRespository.save(_user);
            return new ResponseEntity<Object>(_user, HttpStatus.OK);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("failed to Create specified user:" + e.getCause().getCause().getMessage());

        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(name = "id") Long id, @RequestBody CCustomer userUpdate) {
        // TODO: process POST request
        Optional<CCustomer> _userData = uCuserRespository.findById(id);
        if (_userData.isPresent()) {
            CCustomer _user = _userData.get();
            _user.setFullname(userUpdate.getFullname());
            _user.setEmail(userUpdate.getEmail());
            _user.setPhone(userUpdate.getPhone());
            _user.setAddress(userUpdate.getAddress());
            _user.setUpdated(new Date());
            try {

                return ResponseEntity.ok(uCuserRespository.save(_user));

            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this entity:" + e.getCause().getCause().getMessage());

            }

        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable("id") Long id) {
        Optional<CCustomer> _userData = uCuserRespository.findById(id);
        if (_userData.isPresent()) {
            try {
                uCuserRespository.deleteById(id);
                return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);

            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());

            }
        } else {
            return new ResponseEntity<Object>("User not found", HttpStatus.NOT_FOUND);
        }
    }

}
